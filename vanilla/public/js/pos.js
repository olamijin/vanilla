// have access to cur_pos
let timeoutId = undefined;

window.onload = () => {
	timeoutId = setTimeout(function() {
		if (!window.vanillaPos) {
			injectVanillaIntoPos();
		}
	}, 6000);
};

window.onhashchange = () => {
	timeoutId = setTimeout(function() {
		if (!window.vanillaPos) {
			injectVanillaIntoPos();
		}
	}, 6000);
};

window.onbeforeunload = function(id) {
	clearTimeout(timeoutId);
};

function isPosPage() {
	// this is fragile. #pos can change in the future
	return window.location.toString().includes('#pos');
}

function injectVanillaIntoPos() {
	const pos = isPosPage() ? window.cur_pos : undefined;

	if (pos) {
		const vanilla_print_document = pos.print_document;
		pos.print_document = function(html) {
			if (pos.frm.doc.number_of_prints < 2) {
				vanilla_print_document(html);
				pos.frm.doc.number_of_prints += 1;
			} else {
				window.frappe.show_alert('You are no longer allowed to print this invoice');
			}

		}
	}

	if (isPosPage() && !pos) {
        // page was too slow to load.
		window.frappe.show_alert('Please reload the page for full functionality.');
		frappe.dom.freeze();
	}
}

